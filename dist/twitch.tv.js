// @include "../../jquery.js"
$(document).ready(function () {

  var favStreamers = [
    'Lekkerspelen',
    'hachubby',
    'ethotv',
    'xqcow',
    'lounatuna',
    'philza',
    'm0xyy',
    'mendokusaii',
    'greekgodx',
    'sodapoppin',
    'stormen',
    'aceu'
  ].map(function (v) {
    // bascily sets keys to lowercalse
    return v.toLowerCase();
  });

  console.log(favStreamers)

  // add class to favourite streamers
  function showFavStreamers() {

    $('.preview-card > .tw-relative a').each(function () {

      // get streamer link
      var streamerLink = $(this).attr('href').toLowerCase();

      for (i = 0; i < favStreamers.length; i++) {
        // check if streamer link matches our fav streamers
        if (streamerLink == '/' + favStreamers[i]) {
          $(this).parents('.tw-mg-b-2').addClass('fav-streamer');
        }
      }
    });
    console.log('- Fav streamers loaded');
  }

  // call function every 3 sec
  setInterval(showFavStreamers, 3000)

});